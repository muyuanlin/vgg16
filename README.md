# Final Project for 6.867 Machine Learning.

If the training starts slowly, try using cache:
```
export MXNET_CUDNN_AUTOTUNE_DEFAULT=0
export CUDA_CACHE_MAXSIZE=2147483647
export CUDA_CACHE_DISABLE=0
export CUDA_CACHE_PATH="/home/YOUR_USER_NAME/.nv/ComputeCache"
```