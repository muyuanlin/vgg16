from datetime import datetime
from pytz import timezone
import subprocess
import sys

# Current timezone.
tz = timezone('EST')

# Batch script for running LR/Batch Size experiments.

if __name__ == "__main__":
    lr_base = 0.1
    lr_decay_factor = 0.1
    lr_decay_epoch = "20,30"
    batch_size = 100
    num_conv_batch_norm_sets = 5
    random_mirror = 1
    random_crop = 0
    
    max_random_rotate_angles = [0, 4, 8, 12, 16, 20]

    for max_random_rotate_angle in max_random_rotate_angles:
        # Get time in this timezone.
        timestamp = datetime.now(tz).strftime("%Y%m%dT%H%M%S")
            
        # Generate experiment name.
        experiment_name = "{}_maxrandomrotateangle_{}_randommirror_{}_randomcrop_{}".format(
                timestamp, max_random_rotate_angle, random_mirror, random_crop)
        print("Running experiment {}".format(experiment_name))
            
        args = [
            "python", "process.py",
            experiment_name,
            "--batch-size", str(batch_size),
            "--lr-base", str(lr_base),
            "--lr-decay-epochs", str(lr_decay_epoch),
            "--lr-decay-factor", str(lr_decay_factor),
            "--fc-no-dropout", "--fc-batch-norm",
            "--num-conv-batch-norm-sets", str(num_conv_batch_norm_sets),
            "--random-mirror", str(random_mirror),
            "--random-crop", str(random_crop),
            "--max-random-rotate-angle", str(max_random_rotate_angle)
        ]            
    
        try:
            print(" ".join(args))
            subprocess.check_call(args)
        except subprocess.CalledProcessError:
            print("FAILURE ON " + " ".join(args))
            sys.exit(1)




    
##     hsl_values = [(0, 0, 0), (18, 25, 25), (36, 50, 50), (54, 75, 75), (72, 100, 100)]
## 
##     
##     for i, (random_h, random_s, random_l) in enumerate(hsl_values):
##         # Get time in this timezone.
##         timestamp = datetime.now(tz).strftime("%Y%m%dT%H%M%S")
##             
##         # Generate experiment name.
##         experiment_name = "{}_randomhsl_{}_randommirror_{}_randomcrop_{}".format(
##                 timestamp, i, random_mirror, random_crop)
##         print("Running experiment {}".format(experiment_name))
##             
##         args = [
##             "python", "process.py",
##             experiment_name,
##             "--batch-size", str(batch_size),
##             "--lr-base", str(lr_base),
##             "--lr-decay-epochs", str(lr_decay_epoch),
##             "--lr-decay-factor", str(lr_decay_factor),
##             "--fc-no-dropout", "--fc-batch-norm",
##             "--num-conv-batch-norm-sets", str(num_conv_batch_norm_sets),
##             "--random-mirror", str(random_mirror),
##             "--random-crop", str(random_crop),
##             "--max-random-h", str(random_h),
##             "--max-random-s", str(random_s),
##             "--max-random-l", str(random_l)
##         ]            
##     
##         try:
##             print(" ".join(args))
##             subprocess.check_call(args)
##         except subprocess.CalledProcessError:
##             print("FAILURE ON " + " ".join(args))
##             sys.exit(1)
