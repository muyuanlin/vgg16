from datetime import datetime
from pytz import timezone
import subprocess
import sys

# Current timezone.
tz = timezone('EST')

# Batch script for running LR/Batch Size experiments.

if __name__ == "__main__":
    lr_base = 0.01
    lr_decay_factor = 0.1
    lr_decay_epoch = "20,30"
    batch_size = 50
    fc_dropout_ps = [0.1, 0.3, 0.5, 0.7, 0.9]

    for fc_dropout_p in fc_dropout_ps:
        # Get time in this timezone.
        timestamp = datetime.now(tz).strftime("%Y%m%dT%H%M%S")
            
        # Generate experiment name.
        experiment_name = "{}_fcdropoutp_{}_lrbase_{}_lrdecayepoch_{}_batchsize_{}".format(
            timestamp, fc_dropout_p, lr_base, lr_decay_epoch, batch_size)
        print("Running experiment {}".format(experiment_name))
            
        args = [
            "python", "process.py",
            experiment_name,
            "--batch-size", str(batch_size),
            "--lr-base", str(lr_base),
            "--lr-decay-epochs", str(lr_decay_epoch),
            "--lr-decay-factor", str(lr_decay_factor),
            "--fc-dropout-p", str(fc_dropout_p),
        ]

        try:
            print(" ".join(args))
            subprocess.check_call(args)
        except subprocess.CalledProcessError:
            print("FAILURE ON " + " ".join(args))
            sys.exit(1)
