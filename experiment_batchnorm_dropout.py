from datetime import datetime
from pytz import timezone
import subprocess
import sys

# Current timezone.
tz = timezone('EST')

# Batch script for running LR/Batch Size experiments.

if __name__ == "__main__":
    lr_base = 0.1
    lr_decay_factor = 0.1
    lr_decay_epoch = "20,30"
    batch_size = 100
    num_conv_batch_norm_sets = 5
    fc_dropout_p = 0.7
    

    for fc_no_dropout in range(2):
        for fc_batch_norm in range(2):
            # Get time in this timezone.
            timestamp = datetime.now(tz).strftime("%Y%m%dT%H%M%S")
            
            # Generate experiment name.
            experiment_name = "{}_fcnodropout_{}_fcbatchnorm_{}_lrbase_{}_lrdecayepoch_{}_batchsize_{}".format(
                timestamp, fc_no_dropout, fc_batch_norm, lr_base, lr_decay_epoch, batch_size)
            print("Running experiment {}".format(experiment_name))
            
            args = [
                "python", "process.py",
                experiment_name,
                "--batch-size", str(batch_size),
                "--lr-base", str(lr_base),
                "--lr-decay-epochs", str(lr_decay_epoch),
                "--lr-decay-factor", str(lr_decay_factor),
                "--num-conv-batch-norm-sets", str(num_conv_batch_norm_sets),
                "--fc-dropout-p", str(fc_dropout_p),
                "--aug-level", str(0)
            ]
            if fc_no_dropout:
                args.append("--fc-no-dropout")
            if fc_batch_norm:
                args.append("--fc-batch-norm")
            
            try:
                print(" ".join(args))
                subprocess.check_call(args)
            except subprocess.CalledProcessError:
                print("FAILURE ON " + " ".join(args))
                sys.exit(1)
