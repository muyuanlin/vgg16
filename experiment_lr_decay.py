from datetime import datetime
from pytz import timezone
import subprocess
import sys

# Current timezone.
tz = timezone('EST')

# Batch script for running LR/Batch Size experiments.

if __name__ == "__main__":
    lr_bases = [0.1, 0.05, 0.01]
    batch_sizes = [100, 100, 50]
    lr_decay_factor = 0.1
    lr_decay_epochs = [2, 5, 7, 10, 15, 20]

    for lr_base, batch_size in zip(lr_bases, batch_sizes):
        for lr_decay_epoch in lr_decay_epochs:
            # Get time in this timezone.
            timestamp = datetime.now(tz).strftime("%Y%m%dT%H%M%S")
            
            # Generate experiment name.
            experiment_name = "{}_lrbase_{}_lrdecayepoch_{}_batchsize_{}".format(
                timestamp, lr_base, lr_decay_epoch, batch_size)
            print("Running experiment {}".format(experiment_name))
            
            args = [
                "python", "process.py",
                experiment_name,
                "--batch-size", str(batch_size),
                "--lr-base", str(lr_base),
                "--lr-decay-epochs", str(lr_decay_epoch),
                "--lr-decay-factor", str(lr_decay_factor),
            ]

            try:
                print(" ".join(args))
                subprocess.check_call(args)
            except subprocess.CalledProcessError:
                print("FAILURE ON " + " ".join(args))
                sys.exit(1)
