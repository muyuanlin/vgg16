from datetime import datetime
from pytz import timezone
import subprocess
import sys

# Current timezone.
tz = timezone('EST')

# Batch script for running LR/Batch Size experiments.

if __name__ == "__main__":
    lr = 0.01
    batch_size = 256


    # Get time in this timezone.
    timestamp = datetime.now(tz).strftime("%Y%m%dT%H%M%S")
    
    # Generate experiment name.
    experiment_name = "{}_lr_{}_batchsize_{}".format(
        timestamp, lr, batch_size)
    print("Running experiment {}".format(experiment_name))
    
    args = [
        "python", "process.py",
        experiment_name,
        "--batch-size", str(batch_size),
        "--lr-base", str(lr),
        "--lr-decay-epochs", "",
        "--lr-decay-factor", "1.0",
    ]

    try:
        print(" ".join(args))
        subprocess.check_call(args)
    except subprocess.CalledProcessError:
        print("FAILURE ON " + " ".join(args))
        sys.exit(1)
