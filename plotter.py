from copy import deepcopy
from csv import DictReader as reader
import matplotlib.pyplot as plt
import numpy as np
import os
import pprint
import sys



name_map = {
    "lr": "Learning Rate",
    "batchsize": "Batch Size",
    "lrbase": "Initial Learning Rate",
    "lrdecayepoch": "Learning Rate Decay Epoch",
    "fcdropoutp": "FC Layer Drop Probability",
    "numconvdropoutsets": "Number of Conv Sets with Dropout",
    "convdropoutp": "Conv Layer Drop Probability",
    "numconvbatchnormsets": "Number of Conv Sets with Batch Norm",
    "batchnormafterrelu": "Batch Norm Placed Before ReLu",
    "maxrandomrotateangle": "Max Random Rotation Angle",
    "randommirror": "Random Mirror",
    "randomcrop": "Random Crop",
}

def dict_to_tuple(d):
    return tuple(sorted(d.items(), key=lambda k: k[0]))

def get_file_name(file_path):
    return os.path.splitext(os.path.basename(file_path))[0]

def get_file_ext(file_path):
    return os.path.splitext(os.path.basename(file_path))[1]

def process_file(file_path, settings=None, processed_files=None):
    if settings is None:
        settings = {}
    else:
        settings = deepcopy(settings)
    if processed_files is None:
        processed_files = set()

    # Remove .csv and directories from path, and split on underscore.
    dir_path = os.path.dirname(file_path)
    file_name = get_file_name(file_path)
    splits = file_name.split("_")

    # Remove the trailing _test, _train_epoch, or _val_epoch from name.
    if splits[-1] == "test":
        splits = splits[:-1]
    elif splits[-1] == "epoch":
        splits = splits[:-2]

    # Skip the file if we've already processed it.
    base_name = "_".join(splits)
    if base_name in processed_files:
        return None
    processed_files.add(base_name)

    # Set settings based on file name.
    params = splits[1:]
    for i in range(len(params)//2):
        name = name_map[params[2*i]]
        val = params[2*i+1]
        if "," in val:
            val = "(" + val + ")"
        settings[name] = eval(val)

    output = {}
    output["settings"] = settings

    # Fetch data from files.
    train_csv = os.path.join(dir_path, base_name + "_train_epoch.csv")
    val_csv = os.path.join(dir_path, base_name + "_val_epoch.csv")
    test_csv = os.path.join(dir_path, base_name + "_test.csv")
    
    with open(train_csv, "r") as f:
        epochs = []
        accs = []
        for row in reader(f):
            epoch = int(row["epoch"])
            acc = float(row["accuracy"])
            epochs.append(epoch)
            accs.append(acc)
        output["epochs"] = epochs
        output["train_accuracies"] = accs
        output["train_accuracy"] = accs[-1]
    with open(val_csv, "r") as f:
        accs = []
        for row in reader(f):
            acc = float(row["accuracy"])
            accs.append(acc)
        assert len(output["epochs"]) == len(accs)
        output["val_accuracies"] = accs
        output["val_accuracy"] = accs[-1]
    with open(test_csv, "r") as f:
        accs = []
        for row in reader(f):
            acc = float(row["accuracy"])
            accs.append(acc)
        assert len(accs) == 1
        output["test_accuracy"] = accs[0]

    return output

def process_dir(dir_path, settings=None):
    if settings is None:
        settings = {}

    # Keep track of processed files to prevent duplicates.
    processed_files = set()

    # Gather data from files.
    data = []
    for file_name in os.listdir(dir_path):
        if get_file_ext(file_name) != ".csv":
            continue
        file_path = os.path.join(dir_path, file_name)
        datum = process_file(
            file_path,
            settings=settings,
            processed_files = processed_files)
        if datum is not None:
            data.append(datum)

    # Gather ranges for each setting.
    settings_list = list(data[0]["settings"].keys())
    ranges = {}
    for s in settings_list:
        ranges[s] = set()
    for d in data:
        for s in ranges:
            ranges[s].add(d["settings"][s])
    for s in ranges:
        ranges[s] = list(sorted(list(ranges[s])))

    for s in settings_list:
        if len(ranges[s]) == 0:
            raise Exception("Somehow there are no values for this setting.")
        elif len(ranges[s]) == 1:
            del ranges[s]

    lookup = {}
    for d in data:
        key_dict = {}
        for s in ranges:
            key_dict[s] = d["settings"][s]
        key_tuple = dict_to_tuple(key_dict)
        if key_tuple in lookup:
            lookup[key_tuple].append(d)
        else:
            lookup[key_tuple] = [d]

    total_trials = 0
    for k, v in lookup.items():
        if len(v) == 1:
            lookup[k] = v[0]
            lookup[k]["num_trials"] = 1
        else:
            avg_results = {}
            avg_results["num_trials"] = len(v)
            avg_results["settings"] = v[0]["settings"]
            avg_results["epochs"] = v[0]["epochs"]
            avg_results["train_accuracy"] = np.mean([v[i]["train_accuracy"] for i in range(len(v))])
            avg_results["train_accuracy_std"] = np.std([v[i]["train_accuracy"] for i in range(len(v))]) / np.sqrt(len(v))
            avg_results["val_accuracy"] = np.mean([v[i]["val_accuracy"] for i in range(len(v))])
            avg_results["val_accuracy_std"] = np.std([v[i]["val_accuracy"] for i in range(len(v))]) / np.sqrt(len(v))
            avg_results["test_accuracy"] = np.mean([v[i]["test_accuracy"] for i in range(len(v))])
            avg_results["test_accuracy_std"] = np.std([v[i]["test_accuracy"] for i in range(len(v))]) / np.sqrt(len(v))
            avg_results["train_accuracies"] = np.mean([v[i]["train_accuracies"] for i in range(len(v))], axis=0)
            avg_results["val_accuracies"] = np.mean([v[i]["val_accuracies"] for i in range(len(v))], axis=0)
            
            lookup[k] = avg_results
        
        total_trials += lookup[k]["num_trials"]

    if total_trials != len(data):
        raise Exception("Not all data was put into the lookup table.")

    return ranges, lookup, data



def plot_lr_batch():
    experiment_path = "../experiment_lr_batch/results"
    ranges, lookup, data = process_dir(experiment_path)

    # Validation Accuracy.
    plt.clf()
    for batch_size in ranges[name_map["batchsize"]]:
        lrs = ranges[name_map["lr"]]
        accs = []
        for lr in lrs:
            key_dict = {
                name_map["lr"]: lr,
                name_map["batchsize"]: batch_size,
            }
            key_tuple = dict_to_tuple(key_dict)
            datum = lookup[key_tuple]
            accs.append(datum["val_accuracy"])
        plt.plot(lrs, accs, '.-', label='Batch Size {}'.format(batch_size))
        plt.gca().set_xscale('log')
    
    plt.xlabel(name_map["lr"])
    title = "CIFAR-10 Validation Accuracy"
    plt.ylabel(title)
    plt.axis('tight')
    leg = plt.legend(loc="best", ncol=1, shadow=True, fancybox=True)
    leg.get_frame().set_alpha(0.5)
    plt.show()    


    # Test Accuracy.
    plt.clf()
    for batch_size in ranges[name_map["batchsize"]]:
        lrs = ranges[name_map["lr"]]
        accs = []
        for lr in lrs:
            key_dict = {
                name_map["lr"]: lr,
                name_map["batchsize"]: batch_size,
            }
            key_tuple = dict_to_tuple(key_dict)
            datum = lookup[key_tuple]
            accs.append(datum["test_accuracy"])
        plt.plot(lrs, accs, '.-', label='Batch Size {}'.format(batch_size))
        plt.gca().set_xscale('log')

    plt.xlabel(name_map["lr"])
    title = "STL-10 Test Accuracy"
    plt.ylabel(title)
    plt.axis('tight')
    leg = plt.legend(loc="best", ncol=1, shadow=True, fancybox=True)
    leg.get_frame().set_alpha(0.5)
    plt.show()     



def plot_lr_decay():
    experiment_path = "../experiment_lr_decay/results"
    ranges, lookup, data = process_dir(experiment_path)

    pprint.pprint(lookup.keys())

    # Validation Accuracy.
    plt.clf()
    xaxis = range(0, 39+2, 5)
    plt.xticks(xaxis)
    lr_decay_epochs = ranges[name_map["lrdecayepoch"]]
    accs = []
    for lr_decay_epoch in lr_decay_epochs:
        key_dict = {
            name_map["lrbase"]: 0.01,
            name_map["batchsize"]: 50,
            name_map["lrdecayepoch"]: lr_decay_epoch,
        }
        key_tuple = dict_to_tuple(key_dict)
        datum = lookup[key_tuple]
        accs.append(datum["val_accuracy"])
    plt.plot(lr_decay_epochs, accs, '.-')
    #plt.gca().set_xscale('log')
    
    plt.xlabel(name_map["lrdecayepoch"])
    title = "CIFAR-10 Validation Accuracy"
    plt.ylabel(title)
    plt.axis('tight')
    #leg = plt.legend(loc="best", ncol=1, shadow=True, fancybox=True)
    #leg.get_frame().set_alpha(0.5)
    plt.show()    


    # Test Accuracy.
    plt.clf()
    xaxis = range(0, 39+2, 5)
    plt.xticks(xaxis)
    lr_decay_epochs = ranges[name_map["lrdecayepoch"]]
    accs = []
    for lr_decay_epoch in lr_decay_epochs:
        key_dict = {
            name_map["lrbase"]: 0.01,
            name_map["batchsize"]: 50,
            name_map["lrdecayepoch"]: lr_decay_epoch,
        }
        key_tuple = dict_to_tuple(key_dict)
        datum = lookup[key_tuple]
        accs.append(datum["test_accuracy"])
    plt.plot(lr_decay_epochs, accs, '.-')
    #plt.gca().set_xscale('log')

    plt.xlabel(name_map["lrdecayepoch"])
    title = "STL-10 Test Accuracy"
    plt.ylabel(title)
    plt.axis('tight')
    #leg = plt.legend(loc="best", ncol=1, shadow=True, fancybox=True)
    #leg.get_frame().set_alpha(0.5)
    plt.show()    

def plot_lr_decay_epoch():
    experiment_path = "../experiment_lr_batch/results"
    ranges, lookup, data = process_dir(experiment_path)

    # Validation Accuracy.
    plt.clf()
    key_dict = {
        name_map["lr"]: 0.01,
        name_map["batchsize"]: 50,
    }
    key_tuple = dict_to_tuple(key_dict)
    datum = lookup[key_tuple]
    epochs = datum["epochs"]
    xaxis = range(min(epochs), max(epochs)+2, 5)
    plt.xticks(xaxis)
    accs = datum["val_accuracies"]
    plt.plot(epochs, accs, '-', label='No Decay')

    # Validation accuracy with decay
    experiment_path = "../experiment_lr_decay/results"
    ranges, lookup, data = process_dir(experiment_path)
    
    for lr_decay_epoch in ranges[name_map["lrdecayepoch"]]:
        key_dict = {
            name_map["lrbase"]: 0.01,
            name_map["batchsize"]: 50,
            name_map["lrdecayepoch"]: lr_decay_epoch,
        }
        key_tuple = dict_to_tuple(key_dict)
        datum = lookup[key_tuple]
        accs = datum["val_accuracies"]
        plt.plot(epochs, accs, '-', label="Decay at Epoch {}".format(lr_decay_epoch))
        #plt.gca().set_xscale('log')
    
    plt.xlabel("Epoch")
    title = "CIFAR-10 Validation Accuracy"
    plt.ylabel(title)
    plt.axis('tight')
    leg = plt.legend(loc="best", ncol=1, shadow=True, fancybox=True)
    leg.get_frame().set_alpha(0.5)
    plt.show()



def plot_dropout_p():
    experiment_path = "../experiment_dropout_p_fixed/results"
    ranges, lookup, data = process_dir(experiment_path)

    # Validation Accuracy.
    plt.clf()
    fc_dropout_ps = ranges[name_map["fcdropoutp"]]
    accs = []
    stds = []
    for fc_dropout_p in fc_dropout_ps:
        key_dict = {
            name_map["fcdropoutp"]: fc_dropout_p,
        }
        key_tuple = dict_to_tuple(key_dict)
        datum = lookup[key_tuple]
        accs.append(datum["val_accuracy"])
        stds.append(datum["val_accuracy_std"])
        
    #plt.plot(fc_dropout_ps, accs, '.-')
    plt.errorbar(fc_dropout_ps, accs, stds, fmt='.-')
    #plt.gca().set_xscale('log')
    
    plt.xlabel(name_map["fcdropoutp"])
    title = "CIFAR-10 Validation Accuracy"
    plt.ylabel(title)
    plt.axis('tight')
    #leg = plt.legend(loc="best", ncol=1, shadow=True, fancybox=True)
    #leg.get_frame().set_alpha(0.5)
    plt.show()


    # Test Accuracy.
    plt.clf()
    fc_dropout_ps = ranges[name_map["fcdropoutp"]]
    accs = []
    stds = []
    for fc_dropout_p in fc_dropout_ps:
        key_dict = {
            name_map["fcdropoutp"]: fc_dropout_p,
        }
        key_tuple = dict_to_tuple(key_dict)
        datum = lookup[key_tuple]
        accs.append(datum["test_accuracy"])
        stds.append(datum["test_accuracy_std"])
    #plt.plot(fc_dropout_ps, accs, '.-')
    plt.errorbar(fc_dropout_ps, accs, stds, fmt='.-')
    #plt.gca().set_xscale('log')
    
    plt.xlabel(name_map["fcdropoutp"])
    title = "STL-10 Test Accuracy"
    plt.ylabel(title)
    plt.axis('tight')
    #leg = plt.legend(loc="best", ncol=1, shadow=True, fancybox=True)
    #leg.get_frame().set_alpha(0.5)
    plt.show()

def plot_dropout_p_epoch():
    experiment_path = "../experiment_dropout_p_fixed/results"
    ranges, lookup, data = process_dir(experiment_path)

    # Validation Accuracy.
    plt.clf()
    for fc_dropout_p in ranges[name_map["fcdropoutp"]]:
        if fc_dropout_p == 0.9:
            continue
        key_dict = {
            name_map["fcdropoutp"]: fc_dropout_p,
        }
        key_tuple = dict_to_tuple(key_dict)
        datum = lookup[key_tuple]
        epochs = datum["epochs"]
        xaxis = range(min(epochs), max(epochs)+2, 5)
        plt.xticks(xaxis)   
        accs = datum["val_accuracies"]
        plt.plot(epochs, accs, '-', label="FC Layer Drop Probability {}".format(fc_dropout_p))
        #plt.gca().set_xscale('log')
    
    plt.xlabel("Epoch")
    title = "CIFAR-10 Validation Accuracy"
    plt.ylabel(title)
    plt.axis('tight')
    leg = plt.legend(loc="best", ncol=1, shadow=True, fancybox=True)
    leg.get_frame().set_alpha(0.5)
    plt.show()



def plot_dropout_conv():
    experiment_path = "../experiment_dropout_conv_fixed/results"
    ranges, lookup, data = process_dir(experiment_path)

    # Validation Accuracy.
    plt.clf()
    num_conv_dropout_sets = ranges[name_map["numconvdropoutsets"]]
    accs = []
    stds = []
    for num_conv_dropout_set in num_conv_dropout_sets:
        key_dict = {
            name_map["numconvdropoutsets"]: num_conv_dropout_set,
        }
        key_tuple = dict_to_tuple(key_dict)
        datum = lookup[key_tuple]
        accs.append(datum["val_accuracy"])
        stds.append(datum["test_accuracy_std"])
    plt.errorbar(num_conv_dropout_sets, accs, stds, fmt='.-')
    #plt.plot(num_conv_dropout_sets, accs, '.-')
    #plt.gca().set_xscale('log')
    
    plt.xlabel(name_map["numconvdropoutsets"])
    title = "CIFAR-10 Validation Accuracy"
    plt.ylabel(title)
    plt.axis('tight')
    #leg = plt.legend(loc="best", ncol=1, shadow=True, fancybox=True)
    #leg.get_frame().set_alpha(0.5)
    plt.show()


    # Test Accuracy.
    plt.clf()
    num_conv_dropout_sets = ranges[name_map["numconvdropoutsets"]]
    accs = []
    stds = []
    for num_conv_dropout_set in num_conv_dropout_sets:
        key_dict = {
            name_map["numconvdropoutsets"]: num_conv_dropout_set,
        }
        key_tuple = dict_to_tuple(key_dict)
        datum = lookup[key_tuple]
        accs.append(datum["test_accuracy"])
        stds.append(datum["test_accuracy_std"])
    plt.errorbar(num_conv_dropout_sets, accs, stds, fmt='.-')
    #plt.plot(num_conv_dropout_sets, accs, '.-')
    #plt.gca().set_xscale('log')
    
    plt.xlabel(name_map["numconvdropoutsets"])
    title = "STL-10 Test Accuracy"
    plt.ylabel(title)
    plt.axis('tight')
    #leg = plt.legend(loc="best", ncol=1, shadow=True, fancybox=True)
    #leg.get_frame().set_alpha(0.5)
    plt.show()

def plot_dropout_conv_epoch():
    experiment_path = "../experiment_dropout_conv_fixed/results"
    ranges, lookup, data = process_dir(experiment_path)

    # Validation Accuracy.
    plt.clf()
    for num_conv_dropout_set in ranges[name_map["numconvdropoutsets"]]:
        key_dict = {
            name_map["numconvdropoutsets"]: num_conv_dropout_set,
        }
        key_tuple = dict_to_tuple(key_dict)
        datum = lookup[key_tuple]
        epochs = datum["epochs"]
        xaxis = range(min(epochs), max(epochs)+2, 5)
        plt.xticks(xaxis)   
        accs = datum["val_accuracies"]
        plt.plot(epochs, accs, '-', label="{} Conv Sets with Dropout".format(num_conv_dropout_set))
        #plt.gca().set_xscale('log')
    
    plt.xlabel("Epoch")
    title = "CIFAR-10 Validation Accuracy"
    plt.ylabel(title)
    plt.axis('tight')
    leg = plt.legend(loc="best", ncol=1, shadow=True, fancybox=True)
    leg.get_frame().set_alpha(0.5)
    plt.show()



def plot_batchnorm():
    experiment_path = "../experiment_batchnorm_fixed/results"
    ranges, lookup, data = process_dir(experiment_path)

    # Validation Accuracy.
    plt.clf()
    num_conv_batch_norm_sets = ranges[name_map["numconvbatchnormsets"]]
    accs = []
    for num_conv_batch_norm_set in num_conv_batch_norm_sets:
        key_dict = {
            #name_map["batchnormafterrelu"]: 0,
            name_map["numconvbatchnormsets"]: num_conv_batch_norm_set,
        }
        key_tuple = dict_to_tuple(key_dict)
        datum = lookup[key_tuple]
        accs.append(datum["val_accuracy"])
    plt.plot(num_conv_batch_norm_sets, accs, '.-')
    #plt.gca().set_xscale('log')
    
    plt.xlabel(name_map["numconvbatchnormsets"])
    title = "CIFAR-10 Validation Accuracy"
    plt.ylabel(title)
    plt.axis('tight')
    #leg = plt.legend(loc="best", ncol=1, shadow=True, fancybox=True)
    #leg.get_frame().set_alpha(0.5)
    plt.show()


    # Test Accuracy.
    plt.clf()
    num_conv_batch_norm_sets = ranges[name_map["numconvbatchnormsets"]]
    accs = []
    for num_conv_batch_norm_set in num_conv_batch_norm_sets:
        key_dict = {
            #name_map["batchnormafterrelu"]: 0,
            name_map["numconvbatchnormsets"]: num_conv_batch_norm_set,
        }
        key_tuple = dict_to_tuple(key_dict)
        datum = lookup[key_tuple]
        accs.append(datum["test_accuracy"])
    plt.plot(num_conv_batch_norm_sets, accs, '.-')
    #plt.gca().set_xscale('log')
    
    plt.xlabel(name_map["numconvbatchnormsets"])
    title = "STL-10 Test Accuracy"
    plt.ylabel(title)
    plt.axis('tight')
    #leg = plt.legend(loc="best", ncol=1, shadow=True, fancybox=True)
    #leg.get_frame().set_alpha(0.5)
    plt.show()

def plot_batchnorm_epoch():
    experiment_path = "../experiment_batchnorm_fixed/results"
    ranges, lookup, data = process_dir(experiment_path)

    # Validation Accuracy.
    plt.clf()
    for num_conv_batch_norm_set in ranges[name_map["numconvbatchnormsets"]]:
        key_dict = {
            #name_map["batchnormafterrelu"]: 0,
            name_map["numconvbatchnormsets"]: num_conv_batch_norm_set,
        }
        key_tuple = dict_to_tuple(key_dict)
        datum = lookup[key_tuple]
        epochs = datum["epochs"]
        xaxis = range(min(epochs), max(epochs)+2, 5)
        plt.xticks(xaxis)   
        accs = datum["val_accuracies"]
        plt.plot(epochs, accs, '-', label="{} Conv Sets with Batch Norm".format(num_conv_batch_norm_set))
        #plt.gca().set_xscale('log')
    
    plt.xlabel("Epoch")
    title = "CIFAR-10 Validation Accuracy"
    plt.ylabel(title)
    plt.axis('tight')
    leg = plt.legend(loc="best", ncol=1, shadow=True, fancybox=True)
    leg.get_frame().set_alpha(0.5)
    plt.show()

def plot_rotation():
    experiment_path = "../experiment_dataaug_2_fixed_2/results"
    ranges, lookup, data = process_dir(experiment_path)

    # Validation Accuracy.
    plt.clf()
    max_random_rotate_angles = ranges[name_map["maxrandomrotateangle"]]
    accs = []
    stds = []
    for max_random_rotate_angle in max_random_rotate_angles:
        key_dict = {
            name_map["maxrandomrotateangle"]: max_random_rotate_angle,
        }
        key_tuple = dict_to_tuple(key_dict)
        datum = lookup[key_tuple]
        accs.append(datum["val_accuracy"])
        stds.append(datum["val_accuracy_std"])
    plt.errorbar(max_random_rotate_angles, accs, stds, fmt='.-')
    plt.plot(max_random_rotate_angles, accs, '.-')
    #plt.gca().set_xscale('log')
    
    plt.xlabel(name_map["maxrandomrotateangle"])
    title = "CIFAR-10 Validation Accuracy"
    plt.ylabel(title)
    plt.axis('tight')
    #leg = plt.legend(loc="best", ncol=1, shadow=True, fancybox=True)
    #leg.get_frame().set_alpha(0.5)
    plt.show()


    # Test Accuracy.
    plt.clf()
    max_random_rotate_angles = ranges[name_map["maxrandomrotateangle"]]
    accs = []
    stds = []
    for max_random_rotate_angle in max_random_rotate_angles:
        key_dict = {
            name_map["maxrandomrotateangle"]: max_random_rotate_angle,
        }
        key_tuple = dict_to_tuple(key_dict)
        datum = lookup[key_tuple]
        accs.append(datum["test_accuracy"])
        stds.append(datum["test_accuracy_std"])
    plt.errorbar(max_random_rotate_angles, accs, stds, fmt='.-')
    plt.plot(max_random_rotate_angles, accs, '.-')
    #plt.gca().set_xscale('log')
    
    plt.xlabel(name_map["maxrandomrotateangle"])
    title = "STL-10 Test Accuracy"
    plt.ylabel(title)
    plt.axis('tight')
    #leg = plt.legend(loc="best", ncol=1, shadow=True, fancybox=True)
    #leg.get_frame().set_alpha(0.5)
    plt.show()

def plot_rotation_epoch():
    experiment_path = "../experiment_dataaug_2_fixed_2/results"
    ranges, lookup, data = process_dir(experiment_path)

    # Validation Accuracy.
    plt.clf()
    for max_random_rotate_angle in ranges[name_map["maxrandomrotateangle"]]:
        key_dict = {
            name_map["maxrandomrotateangle"]: max_random_rotate_angle,
        }
        key_tuple = dict_to_tuple(key_dict)
        datum = lookup[key_tuple]
        epochs = datum["epochs"]
        xaxis = range(min(epochs), max(epochs)+2, 5)
        plt.xticks(xaxis)   
        accs = datum["val_accuracies"]
        plt.plot(epochs, accs, '-', label="Max ${}^\circ$ Random Rotations".format(max_random_rotate_angle))
        #plt.gca().set_xscale('log')
    
    plt.xlabel("Epoch")
    title = "CIFAR-10 Validation Accuracy"
    plt.ylabel(title)
    plt.axis('tight')
    leg = plt.legend(loc="best", ncol=1, shadow=True, fancybox=True)
    leg.get_frame().set_alpha(0.5)
    plt.show()


def plot_baseline_epoch():
    experiment_path = "../experiment_baseline/results"
    ranges, lookup, data = process_dir(experiment_path)

    # Train and Validation Accuracy.
    plt.clf()
    key_dict = {}
    key_tuple = dict_to_tuple(key_dict)
    datum = lookup[key_tuple]
    epochs = datum["epochs"]
    xaxis = range(min(epochs), max(epochs)+2, 5)
    plt.xticks(xaxis)
    train_accs = datum["train_accuracies"]
    val_accs = datum["val_accuracies"]
    plt.plot(epochs, train_accs, '-', label="CIFAR-10 Train")    
    plt.plot(epochs, val_accs, '-', label="CIFAR-10 Validation")
    #plt.gca().set_xscale('log')
    
    plt.xlabel("Epoch")
    plt.ylabel("Accuracy")
    plt.axis('tight')
    leg = plt.legend(loc="best", ncol=1, shadow=True, fancybox=True)
    leg.get_frame().set_alpha(0.5)
    plt.show()


def plot_efficiency():
    batch_sizes = [10, 50, 100, 500, 1000]
    sampling_rates = [1020.64, 3334.96, 4880.85, 7570.80, 8039.56]

    # Train and Validation Accuracy.
    plt.plot(batch_sizes, sampling_rates)
    plt.gca().set_xscale('log')
    
    plt.xlabel("Batch Size")
    plt.ylabel("Sampling Rate (samples/sec)")
    plt.axis('tight')
    #leg = plt.legend(loc="best", ncol=1, shadow=True, fancybox=True)
    #leg.get_frame().set_alpha(0.5)
    plt.show()






if __name__ == '__main__':
    #plot_lr_batch()
    #plot_lr_decay()
    #plot_lr_decay_epoch()
    #plot_dropout_p()
    #plot_dropout_p_epoch()
    #plot_dropout_conv()
    #plot_dropout_conv_epoch()
    #plot_batchnorm()
    #plot_batchnorm_epoch()
    #plot_rotation()
    #plot_rotation_epoch()
    #plot_baseline_epoch()
    #plot_efficiency()
    sys.exit(0)
