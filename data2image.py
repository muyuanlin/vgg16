import cv2
import logging
import matplotlib.pyplot as plt
import pickle
import json
import numpy as np
import os
import shutil
from scipy import misc
import sys

cifar10_skipped_labels = ["frog"]
stl10_skipped_labels = ["monkey"]



def read_pickle(file_path):
    with open(file_path, 'rb') as f:
        return pickle.load(f, encoding='bytes')

def cifar10_to_image(cifar10_data_path, root_path, force_overwrite=False):
    # Extract class names
    meta_name = os.path.join(cifar10_data_path, "batches.meta")
    d = read_pickle(meta_name)
    label_names_raw = d[b'label_names']
    
    # For Python 3, want Unicode strings, so need to convert.
    label_names = [s.decode('utf-8') for s in label_names_raw]
    
    # Create directories in root path.
    root_path = os.path.abspath(root_path)
    for label in label_names:
        if label in cifar10_skipped_labels:
            continue
        label_path = os.path.join(root_path, label)
        try:
            os.makedirs(label_path)
        except FileExistsError:
            if force_overwrite:
                shutil.rmtree(label_path)
                os.makedirs(label_path)
            else:
                print("CIFAR-10 already extracted.")
                return

    # Go through data files and convert each to image.
    num_batches = 5
    idx = 0
    for i in range(1, 6):
        train_path = os.path.join(cifar10_data_path, "data_batch_{}".format(i))
        idx = process_cifar10_batch_file(idx, train_path, root_path, label_names)

    test_path = os.path.join(cifar10_data_path, "test_batch")
    process_cifar10_batch_file(idx, test_path, root_path, label_names)
    
def process_cifar10_batch_file(idx_start, batch_path, root_path, label_names):
    d = read_pickle(batch_path)
    data_raw = d[b'data']
    numeric_labels_raw = d[b'labels']

    # The data is originally formatted as (N, C, H, W) (row-major order).
    data = np.reshape(data_raw, (-1, 3, 32, 32))

    # Change it into (N, H, W, C) for cv2.
    data = np.transpose(data, (0, 2, 3, 1))

    # Change from RGB to BGR for cv2.
    data = np.flip(data, -1)

    # Convert labels to numpy array.
    numeric_labels = np.array(numeric_labels_raw)
    num_images = data.shape[0]

    # Write out images to disk.
    for i in range(num_images):
        label = label_names[numeric_labels[i]]
        if label in cifar10_skipped_labels:
            continue
        file_path = os.path.join(root_path, label, '{0:06d}.png'.format(i + idx_start))
        cv2.imwrite(file_path, data[i, ...])

    return num_images + idx_start


def stl10_to_image(stl10_data_path, root_path, force_overwrite=False):
    # Extract class names
    meta_name = os.path.join(stl10_data_path, "class_names.txt")
    with open(meta_name, "r") as f:
        label_names = [line.strip() for line in f.readlines()]

    # To be consistent with CIFAR-10, we change the "car" label to "automobile".
    if label_names[2] == "car":
        label_names[2] = "automobile"
    else:
        print("Class labels for STL-10 have been manually edited, aborting...")
        sys.exit(1)

    # Create directories in root path.
    root_path = os.path.abspath(root_path)
    for label in label_names:
        if label in stl10_skipped_labels:
            continue
        label_path = os.path.join(root_path, label)
        try:
            os.makedirs(label_path)
        except FileExistsError:
            if force_overwrite:
                shutil.rmtree(label_path)
                os.makedirs(label_path)
            else:
                print("STL-10 already extracted.")
                return

    # Go through data files and convert each to image.
    train_data_path = os.path.join(stl10_data_path, "train_X.bin")
    train_label_path = os.path.join(stl10_data_path, "train_y.bin") 
    test_data_path = os.path.join(stl10_data_path, "test_X.bin")
    test_label_path = os.path.join(stl10_data_path, "test_y.bin")

    idx = 0
    idx = process_stl10_batch_file(idx, train_data_path, train_label_path,
                                   root_path, label_names)
    idx = process_stl10_batch_file(idx, test_data_path, test_label_path,
                                   root_path, label_names)
    

def process_stl10_batch_file(idx_start, batch_data_path, batch_label_path,
                             root_path, label_names):
    data_raw = np.fromfile(batch_data_path, dtype=np.uint8)
    numeric_labels_raw = np.fromfile(batch_label_path, dtype=np.uint8)

    # The data is originally formatted as (N, C, W, H) (column-major order).
    data = np.reshape(data_raw, (-1, 3, 96, 96))

    # Change it into (N, H, W, C) for cv2.
    data = np.transpose(data, (0, 3, 2, 1))

    # Change from RGB to BGR for cv2.
    data = np.flip(data, -1)
    num_images = data.shape[0]

    # Resize all images to 32x32.
    data_resize = np.zeros((num_images, 32, 32, 3), dtype=np.uint8)
    for i in range(num_images):
        data_resize[i, ...] = cv2.resize(data[i, ...], (32, 32),
                                         interpolation = cv2.INTER_AREA)
    data = data_resize
    
    # Labels are 1-indexed, so subtract 1.
    numeric_labels = numeric_labels_raw - 1;

    # Write out images to disk.
    for i in range(num_images):
        label = label_names[numeric_labels[i]]
        if label in stl10_skipped_labels:
            continue
        file_path = os.path.join(root_path, label, '{0:06d}.png'.format(i + idx_start))
        cv2.imwrite(file_path, data[i, ...])

    return num_images + idx_start
