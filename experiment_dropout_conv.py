from datetime import datetime
from pytz import timezone
import subprocess
import sys

# Current timezone.
tz = timezone('EST')

# Batch script for running LR/Batch Size experiments.

if __name__ == "__main__":
    lr_base = 0.01
    lr_decay_factor = 0.1
    lr_decay_epoch = "20,30"
    batch_size = 50
    fc_dropout_p = 0.7
    conv_dropout_p = 0.2
    

    for num_conv_dropout_sets in range(6):
        # Get time in this timezone.
        timestamp = datetime.now(tz).strftime("%Y%m%dT%H%M%S")
            
        # Generate experiment name.
        experiment_name = "{}_numconvdropoutsets_{}_convdropoutp_{}_fcdropoutp_{}_lrbase_{}_lrdecayepoch_{}_batchsize_{}".format(
            timestamp, num_conv_dropout_sets, conv_dropout_p, fc_dropout_p, lr_base, lr_decay_epoch, batch_size)
        print("Running experiment {}".format(experiment_name))
            
        args = [
            "python", "process.py",
            experiment_name,
            "--batch-size", str(batch_size),
            "--lr-base", str(lr_base),
            "--lr-decay-epochs", str(lr_decay_epoch),
            "--lr-decay-factor", str(lr_decay_factor),
            "--fc-dropout-p", str(fc_dropout_p),
            "--num-conv-dropout-sets", str(num_conv_dropout_sets),
            "--conv-dropout-p", str(conv_dropout_p),
        ]

        try:
            print(" ".join(args))
            subprocess.check_call(args)
        except subprocess.CalledProcessError:
            print("FAILURE ON " + " ".join(args))
            sys.exit(1)
