from datetime import datetime
from pytz import timezone
import subprocess
import sys

# Current timezone.
tz = timezone('EST')

# Batch script for running LR/Batch Size experiments.

if __name__ == "__main__":
    lr_base = 0.1
    lr_decay_factor = 0.1
    lr_decay_epoch = "20,30"
    batch_size = 100
    num_conv_batch_norm_sets = 5

    for random_mirror in range(2):
        for random_crop in range(2):
            # Get time in this timezone.
            timestamp = datetime.now(tz).strftime("%Y%m%dT%H%M%S")
            
            # Generate experiment name.
            experiment_name = "{}_randommirror_{}_randomcrop_{}_numconvbatchnormsets_{}_lrbase_{}_lrdecayepoch_{}_batchsize_{}".format(
                timestamp, random_mirror, random_crop, num_conv_batch_norm_sets, lr_base, lr_decay_epoch, batch_size)
            print("Running experiment {}".format(experiment_name))
            
            args = [
                "python", "process.py",
                experiment_name,
                "--batch-size", str(batch_size),
                "--lr-base", str(lr_base),
                "--lr-decay-epochs", str(lr_decay_epoch),
                "--lr-decay-factor", str(lr_decay_factor),
                "--fc-no-dropout", "--fc-batch-norm",
                "--num-conv-batch-norm-sets", str(num_conv_batch_norm_sets),
                "--random-mirror", str(random_mirror),
                "--random-crop", str(random_crop)
            ]
            if random_crop:
                args.append("--max-random-scale")
                args.append(str(1.5))
    
            try:
                print(" ".join(args))
                subprocess.check_call(args)
            except subprocess.CalledProcessError:
                print("FAILURE ON " + " ".join(args))
                sys.exit(1)
