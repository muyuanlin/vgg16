import os
import math
import logging

import csv
import sys
from tensorboardX import SummaryWriter

logging.basicConfig(level=logging.DEBUG)

fieldnames_epoch = ['epoch', 'accuracy']
fieldnames_test = ['accuracy']

####################################################################################################
# CSV/TensorboardX Setup/Teardown
####################################################################################################


def create_csv_files(base_dir, prefix):
    if not os.path.exists(base_dir):
        os.makedirs(base_dir)
        
    train_epoch_csv = os.path.join(base_dir, prefix +'_train_epoch.csv')
    val_epoch_csv = os.path.join(base_dir, prefix + '_val_epoch.csv')
    test_csv = os.path.join(base_dir, prefix + '_test.csv')

    if os.path.exists(train_epoch_csv) or \
       os.path.exists(val_epoch_csv) or \
       os.path.exists(test_csv):
        print("You're trying to overwrite a csv results file!")
        sys.exit(1)
    
    with open(train_epoch_csv, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames_epoch)
        writer.writeheader()
    with open(val_epoch_csv, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames_epoch)
        writer.writeheader()
    with open(test_csv, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames_test)
        writer.writeheader()
    return train_epoch_csv, val_epoch_csv, test_csv


def open_tensorboardx_writer(base_dir, prefix):
    tensorboardx_dir = os.path.join(base_dir, prefix)
    if not os.path.exists(tensorboardx_dir):
        os.makedirs(tensorboardx_dir)

    writer = SummaryWriter(tensorboardx_dir)
    return writer


def close_tensorboardx_writer(writer, base_dir, prefix):
    tensorboardx_dir = os.path.join(base_dir, prefix)
    writer.export_scalars_to_json(os.path.join(tensorboardx_dir, "all_scalars.json"))
    writer.close()

    
####################################################################################################
# CSV/TensorboardX Writers
####################################################################################################

def write_csv_train(filename, epoch, acc):
    with open(filename, 'a') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames_epoch)
        writer.writerow({
            'epoch':epoch,
            'accuracy': acc,
            })

def write_csv_val(filename, epoch, acc):
    with open(filename, 'a') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames_epoch)
        writer.writerow({
            'epoch':epoch,
            'accuracy': acc,
            })

def write_csv_test(filename, acc):
    with open(filename, 'a') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames_test)
        writer.writerow({
            'accuracy': acc,
            })

def write_tensorboardx_train(writer, epoch, acc):
    writer.add_scalar('train/accuracy', acc, epoch)

def write_tensorboardx_val(writer, epoch, acc):
    writer.add_scalar('val/accuracy', acc, epoch)

def write_tensorboardx_test(writer, acc):
    writer.add_scalar('test/accuracy', acc)

####################################################################################################
# CSV/TensorboardX Writers
####################################################################################################


    

## def log_train_metric(period, filename=None, writer=None, auto_reset=False):
##     """Callback to log the training evaluation result every period.
## 
##     Parameters
##     ----------
##     period : int
##         The number of batch to log the training evaluation metric.
##     auto_reset : bool
##         Reset the metric after each log.
## 
##     Returns
##     -------
##     callback : function
##         The callback function that can be passed as iter_epoch_callback to fit.
##     """
##     def _callback(param):
##         print("log_train_metric callback called with nbatch={}".format(param.nbatch))
##         if param.nbatch % period == 0 and param.eval_metric is not None:
##             name_value = param.eval_metric.get_name_value()
##             for name, value in name_value:
##                 if not math.isnan(value) and name.lower() == "accuracy":
##                     # logging.info('------Iter[%d] Batch[%d] Train-%s=%f',
##                     #              param.epoch, param.nbatch, name, value)
##                     epoch = param.epoch
##                     accuracy = value
## 
##                     if filename is not None:
##                         write_csv_train(filename, epoch, accuracy)
## 
##                     if writer is not None:
##                         write_tensorboardx_train(writer, epoch, accuracy)
## 
##             if auto_reset:
##                 param.eval_metric.reset()
##     return _callback


## def log_eval_metric(period, filename=None, writer=None, auto_reset=False):
##     """Callback to log the training evaluation result every period.
## 
##     Parameters
##     ----------
##     period : int
##         The number of batch to log the training evaluation metric.
##     auto_reset : bool
##         Reset the metric after each log.
## 
##     Returns
##     -------
##     callback : function
##         The callback function that can be passed as iter_epoch_callback to fit.
##     """
##     
##     def _callback(param):
##         print("log_eval_metric callback called with nbatch={}".format(param.nbatch))
##         if param.nbatch % period == 0 and param.eval_metric is not None:
##             name_value = param.eval_metric.get_name_value()
##             for name, value in name_value:
##                 if not math.isnan(value) and name.lower() == "accuracy":
##                     # logging.info('------Iter[%d] Batch[%d] Eval-%s=%f',
##                     #              param.epoch, param.nbatch, name, value)
##                     epoch = param.epoch
##                     accuracy = value
## 
##                     if filename is not None:
##                         write_csv_val(filename, epoch, accuracy)
## 
##                     if writer is not None:
##                         write_tensorboardx_val(writer, epoch, accuracy)
##                         
##             if auto_reset:
##                 param.eval_metric.reset()
##     return _callback


def log_train_metric(filename=None, writer=None):
    def _callback(epoch, name_value):
        for name, value in name_value:
            if not math.isnan(value) and name.lower() == "accuracy":
                # logging.info('------Iter[%d] Batch[%d] Train-%s=%f',
                #              param.epoch, param.nbatch, name, value)
                accuracy = value
                
                if filename is not None:
                    write_csv_train(filename, epoch, accuracy)
                    
                if writer is not None:
                    write_tensorboardx_train(writer, epoch, accuracy)

    return _callback


def log_eval_metric(filename=None, writer=None):
    def _callback(epoch, name_value):
        for name, value in name_value:
            if not math.isnan(value) and name.lower() == "accuracy":
                # logging.info('------Iter[%d] Batch[%d] Eval-%s=%f',
                #              param.epoch, param.nbatch, name, value)
                accuracy = value
                
                if filename is not None:
                    write_csv_val(filename, epoch, accuracy)

                if writer is not None:
                    write_tensorboardx_val(writer, epoch, accuracy)

    return _callback
