import argparse
import logging
from matplotlib import pyplot as plt
from matplotlib.pyplot import draw, plot, show, ion
import json
import mxnet as mx
import numpy as np
import pickle
import pprint
import os
import sys
import tensorboardX
import time


from downloader import download_cifar10_rec
import downloader
import data
import data2image
import module_callback
import im2rec
from util import*


np.random.seed(int(time.time()))
mx.random.seed(int(time.time()))

logging.basicConfig(level=logging.DEBUG)


# Use this flag to skip preprocessing steps, if we know it's already done.
skip_preprocessing = True

# Filesystem Settings
data_base_path = "data"
model_base_path = "model"
results_base_path = "results"
tensorboardx_base_path = "logs"

# Get CIFAR-10 and STL-10 base paths.
cifar10_base_path = os.path.join(data_base_path, "CIFAR-10")
stl10_base_path = os.path.join(data_base_path, "STL-10")
vgg16_base_path = os.path.join(model_base_path, "VGG16")

# Fetch file paths to datasets, downloading if needed.
if not skip_preprocessing:
    downloader.download_cifar10(cifar10_base_path)
cifar10_data_path = downloader.get_cifar10_data_path(cifar10_base_path)

if not skip_preprocessing:
    downloader.download_vgg16(vgg16_base_path)
vgg16_model_path = downloader.get_vgg16_model_path(vgg16_base_path)

if not skip_preprocessing:
    downloader.download_stl10(stl10_base_path)
stl10_data_path = downloader.get_stl10_data_path(stl10_base_path)

# Extract dataset images to directories.
cifar10_images_path = os.path.join(cifar10_base_path, "images")
if not skip_preprocessing:
    data2image.cifar10_to_image(cifar10_data_path,
                                cifar10_images_path,
                                force_overwrite=False)

stl10_images_path = os.path.join(stl10_base_path, "images")
if not skip_preprocessing:
    data2image.stl10_to_image(stl10_data_path,
                              stl10_images_path,
                              force_overwrite=False)

# Convert images to RecordIO files.
if not skip_preprocessing:
    im2rec.cifar10_to_lst(cifar10_base_path,
                          cifar10_images_path,
                          force_overwrite=False)
    im2rec.stl10_to_lst(stl10_base_path,
                        stl10_images_path,
                        force_overwrite=False)
    im2rec.cifar10_to_rec(cifar10_base_path,
                          cifar10_images_path,
                          force_overwrite=False)
    im2rec.stl10_to_rec(stl10_base_path,
                        stl10_images_path,
                        force_overwrite=False)

# Fetch the paths of the .rec and .idx files.
cifar10_train_rec_path = im2rec.get_cifar10_train_rec(cifar10_base_path)
cifar10_train_idx_path = im2rec.get_cifar10_train_idx(cifar10_base_path)
cifar10_test_rec_path  = im2rec.get_cifar10_test_rec(cifar10_base_path)
cifar10_test_idx_path  = im2rec.get_cifar10_test_idx(cifar10_base_path)
stl10_rec_path  = im2rec.get_stl10_rec(stl10_base_path)
stl10_idx_path  = im2rec.get_stl10_idx(stl10_base_path)

cifar10_train_num_examples = im2rec.get_cifar10_train_num_examples(cifar10_base_path)
cifar10_test_num_examples = im2rec.get_cifar10_test_num_examples(cifar10_base_path)
stl10_num_examples = im2rec.get_stl10_num_examples(stl10_base_path)


# Description of VGG16 model, as taken from
# http://data.dmlc.ml/models/imagenet/vgg/vgg16-symbol.json
def vgg16_model(args, data, label):
    # Load the model description from JSON file.
    with open(vgg16_model_path, "r") as f:
        vgg16_json = json.load(f)


    ## args.fc_batch_norm
    ## args.batch_norm_after_relu

    # Keep track of whether FC layers have started.
    is_start_fc = False
        
    # Keep track of conv layer count to know when to apply certain transformations.
    conv_count = 0

    # Offsets for the conv sets.
    conv_sets = [0, 2, 4, 7, 10, 13]
    conv_count_dropout = conv_sets[5 - args.num_conv_dropout_sets]
    conv_count_batch_norm = conv_sets[5 - args.num_conv_batch_norm_sets]

    # Set up input as data.
    sym = data

    # Iterate through layers of VGG16 and select first half.
    for layer in vgg16_json["nodes"]:
        if layer["op"] == "Convolution":
            # Create Batch Normalization layer.
            if args.batch_norm_after_relu:
                if conv_count+1 > conv_count_batch_norm:
                    sym = mx.sym.BatchNorm(
                        name = layer["name"] + "_bn",
                        data = sym)
            
            # Create 2D conv layer.
            sym = mx.sym.Convolution(
                name = layer["name"],
                data = sym,
                num_filter = layer["param"]["num_filter"],
                kernel = layer["param"]["kernel"],
                pad = layer["param"]["pad"])

            # Increment the number of conv layers encountered.
            conv_count += 1

            # Create Batch Normalization layer.
            if not args.batch_norm_after_relu:
                if conv_count > conv_count_batch_norm:
                    sym = mx.sym.BatchNorm(
                        name = layer["name"] + "_bn",
                        data = sym)
        elif layer["op"] == "Activation":
            # Create ReLu activation layer.
            sym = mx.sym.Activation(
                name = layer["name"],
                data = sym,
                act_type = layer["param"]["act_type"])

            # Create Dropout layer.
            if is_start_fc:
                if not args.fc_no_dropout:
                    sym = mx.sym.Dropout(
                        name = layer["name"] + "_dropout",
                        data = sym,
                        p = args.fc_dropout_p) #originally layer["param"]["p"]
            else:
                if conv_count > conv_count_dropout:
                    sym = mx.sym.Dropout(
                        name = layer["name"] + "_dropout",
                        data = sym,
                        p = args.conv_dropout_p) #originally layer["param"]["p"]
            
        elif layer["op"] == "Pooling":
            # Create MaxPooling layer.
            sym = mx.sym.Pooling(
                name = layer["name"],
                data = sym,
                pool_type = layer["param"]["pool_type"],
                kernel = layer["param"]["kernel"],
                stride = layer["param"]["stride"])
        elif layer["op"] == "Flatten":
            # Create flatten layer.
            sym = mx.sym.flatten(
                name = layer["name"],
                data = sym)
        elif layer["op"] == "FullyConnected":
            # Standard VGG16 has 1000 output nodes, only want 10.
            if layer["name"] == "fc8":
                num_hidden = 9
            else:
                num_hidden = layer["param"]["num_hidden"]

            # Create Batch Normalization layer.
            if args.batch_norm_after_relu:
                if args.fc_batch_norm:
                    sym = mx.sym.BatchNorm(
                        name = layer["name"] + "_bn",
                        data = sym)
            
            # Create FullyConnected layer.
            sym = mx.sym.FullyConnected(
                name = layer["name"],
                data = sym,
                num_hidden = num_hidden)

            # Indicate that FC layers have started.
            is_start_fc = True

            # if layer["name"] != "fc8":
            if not args.batch_norm_after_relu:
                if args.fc_batch_norm:
                    sym = mx.sym.BatchNorm(
                        name = layer["name"] + "_bn",
                        data = sym)
        elif layer["op"] == "Dropout":
            # We now create dropout layers after activation layers.
            pass
                
        elif layer["op"] == "SoftmaxOutput":
            # Create SoftmaxOutput layer.
            sym = mx.sym.SoftmaxOutput(
                name = layer["name"],
                label = label,
                data = sym)
        elif layer["op"] == "null":
            continue
        else:
            print(layer["op"])
            raise Exception("Didn't handle op/layer type {}".format(layer["op"]))

    return sym

if __name__ == "__main__":
    # Parse arguments.
    parser = argparse.ArgumentParser(description="transfer learning pipeline",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    data.add_data_args(parser)
    data.add_model_args(parser)
    data.add_data_aug_args(parser)
    
    aug_level = parser.parse_args().aug_level
    data.set_data_aug_level(parser, aug_level)
    
    args = parser.parse_args()

    print()
    print("Parser Arguments:")
    pprint.pprint(vars(args)) 
    print()

    # Create RecordIO iterators for datasets.
    (train, val, test) = data.get_rec_iter(args, cifar10_base_path, stl10_base_path)

    # Create model.
    data = mx.sym.Variable("data")
    label = mx.sym.Variable("softmax_label")
    sym = vgg16_model(args, data, label)
    #pltn = mx.viz.plot_network(model, node_attrs={"shape":"oval","fixedsize":"false"})
    #pltn.view("VGG16")

    # Print model.
    mx.viz.print_summary(
        sym,
        shape = {
            "data": (args.batch_size, 3, 32, 32)
        }
    )

    # Create MXnet Module with model.
    mod = module_callback.Module_Callback(
        symbol = sym,
        context = mx.gpu(),
        data_names = ["data"],
        label_names = ["softmax_label"])

    # Create initializer, it's important to use Xavier for VGG16.
    initializer = mx.initializer.Xavier(
        rnd_type = "uniform",
        factor_type = "avg")
    # initializer = mx.initializer.MSRAPrelu(
    #     factor_type='avg', slope=0.25)

    # Learning schedule
    lr_base = args.lr_base
    if len(args.lr_decay_epochs.strip()) or args.lr_decay_factor != 1.0:
        lr_decay_epochs = [float(i) for i in args.lr_decay_epochs.split(',')]
        lr_decay_epochs.append(10000) # append a large number so the scheduler processes the last value in list
        lr_decay_factor = args.lr_decay_factor
        lr_decay_iters = [int(e * cifar10_train_num_examples / args.batch_size) for e in lr_decay_epochs]
        print("learning rate schedule: ", lr_decay_epochs)

        lr_scheduler = mx.lr_scheduler.MultiFactorScheduler(lr_decay_iters, lr_decay_factor)
    else:
        lr_scheduler = None

    # Optimizer Params
    optimizer_params = {'momentum': 0.9, # Typically between 0.5 and 0.9
                        'wd': 0.0005,  # "Weight Decay", a.k.a. L2 regularization penalty (this is probably fine at 0.0005)
                        'learning_rate': lr_base,
                        'lr_scheduler': lr_scheduler,
                        'rescale_grad': (1.0 / args.batch_size), # This is important to keep.
                        'clip_gradient': 5} # Not sure whether to keep, basically bounds the gradient to [-clip_gradient, clip_gradient]

    # Setup CSV files and TensorboardX writers for outputting results.
    train_epoch_csv, val_epoch_csv, test_csv = create_csv_files(results_base_path, args.experiment_name)
    writer = open_tensorboardx_writer(tensorboardx_base_path, args.experiment_name)
    
    # Callbacks for fit(), the speedometer prints out info every few epochs.
    batch_end_callback = mx.callback.Speedometer(args.batch_size, 10)
    train_metric_callback = log_train_metric(filename=train_epoch_csv, writer=writer)
    eval_metric_callback = log_eval_metric(filename=val_epoch_csv, writer=writer)

    # Callback for saving weights at the end of each epoch.
    # epoch_end_callback = mx.callback.do_checkpoint("cifar10_best", period=5)
    
    # fit the module
    mod.fit(train,
            eval_data = val,
            batch_end_callback = batch_end_callback,
            optimizer = "sgd",
            initializer = initializer,
            optimizer_params = optimizer_params,
            eval_metric = "acc",
            num_epoch = args.num_epoch,
            train_metric_callback = train_metric_callback,
            eval_metric_callback = eval_metric_callback)

    score = mod.score(test, ['acc'])
    stl10_acc = score[0][1]
    print("STL-10 Accuracy score is %f" % (stl10_acc))
    
    # Manually write STL-10 accuracy.
    write_csv_test(test_csv, stl10_acc)
    write_tensorboardx_test(writer, stl10_acc)

    # Close tensorboardX writer, which also creates JSON file.
    close_tensorboardx_writer(writer, tensorboardx_base_path, args.experiment_name)
