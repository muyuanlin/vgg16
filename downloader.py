import os
import sys

import requests
import shutil
import tarfile
from tqdm import tqdm
import zipfile



########################################################################

### Directory where you want to download and save the data-set.
### Set this before you start calling any of the functions below.
##data_path = os.path.join("data", "CIFAR-10")
##model_path = os.path.join("model", "vgg16")
##
### URL for the data-set on the internet.
##data_url = "https://www.cs.toronto.edu/~kriz/cifar-10-python.tar.gz"
##model_url = "http://data.mxnet.io/models/imagenet/vgg/"
##filename = "vgg16-symbol.json"
##
##def _get_data_path(filename=""):
##    """
##    Return the full path of a data-file for the data-set.
##    If filename=="" then return the directory of the files.
##    """
##
##    return os.path.join(data_path, "cifar-10-batches-py", filename)
##
##def _get_model_path(filename=""):
##    """
##    Return the full path of a data-file for the data-set.
##    If filename=="" then return the directory of the files.
##    """
##
##    return os.path.join(model_path, filename)

##def _print_download_progress(count, block_size, total_size):
##    """
##    Function used for printing the download progress.
##    Used as a call-back function in maybe_download_and_extract().
##    """
##
##    # Percentage completion.
##    pct_complete = float(count * block_size) / total_size
##
##    # Limit it because rounding errors may cause it to exceed 100%.
##    pct_complete = min(1.0, pct_complete)
##
##    # Status-message. Note the \r which means the line should overwrite itself.
##    msg = "\r- Download progress: {0:.1%}".format(pct_complete)
##
##    # Print it.
##    sys.stdout.write(msg)
##    sys.stdout.flush()

##def download(base_url, filename, download_dir):
##    """
##    Download the given file if it does not already exist in the download_dir.
##    :param base_url: The internet URL without the filename.
##    :param filename: The filename that will be added to the base_url.
##    :param download_dir: Local directory for storing the file.
##    :return: Nothing.
##    """
##
##    # Path for local file.
##    save_path = os.path.join(download_dir, filename)
##
##    # Check if the file already exists, otherwise we need to download it now.
##    if not os.path.exists(save_path):
##        # Check if the download directory exists, otherwise create it.
##        if not os.path.exists(download_dir):
##            os.makedirs(download_dir)
##
##        print("Downloading", filename, "...")
##
##        # Download the file from the internet.
##        url = base_url + filename
##        print(url)
##        file_path, _ = urllib.request.urlretrieve(url=url,
##                                                  filename=save_path,
##                                                  reporthook=_print_download_progress)
##
##        print(" Done!")
def download_file(url, local_fname=None, force_write=False):
    # requests is not default installed
    import requests
    if local_fname is None:
        local_fname = url.split('/')[-1]
    if not force_write and os.path.exists(local_fname):
        return local_fname

    dir_name = os.path.dirname(local_fname)

    if dir_name != "":
        if not os.path.exists(dir_name):
            try:  # try to create the directory if it doesn't exists
                os.makedirs(dir_name)
            except OSError as exc:
                if exc.errno != errno.EEXIST:
                    raise

    r = requests.get(url, stream=True)
    assert r.status_code == 200, "failed to open %s" % url
    with open(local_fname, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk:  # filter out keep-alive new chunks
                f.write(chunk)
    return local_fname


def download_cifar10_rec():
    data_dir = "data"
    fnames = (os.path.join(data_dir, "cifar10_train.rec"),
              os.path.join(data_dir, "cifar10_val.rec"))
    download_file('http://data.mxnet.io/data/cifar10/cifar10_val.rec', fnames[1])
    download_file('http://data.mxnet.io/data/cifar10/cifar10_train.rec', fnames[0])
    return fnames

def maybe_download_and_extract(url, download_dir):
    """
    Download and extract the data if it doesn't already exist.
    Assumes the url is a tar-ball file.
    :param url:
        Internet URL for the tar-file to download.
        Example: "https://www.cs.toronto.edu/~kriz/cifar-10-python.tar.gz"
    :param download_dir:
        Directory where the downloaded file is saved.
        Example: "data/CIFAR-10/"
    :return:
        Nothing.
    """

    # Filename for saving the file downloaded from the internet.
    # Use the filename from the URL and add it to the download_dir.
    filename = url.split('/')[-1]
    file_path = os.path.join(download_dir, filename)

    # Fetch file size.
    expected_file_size = int(requests.get(url, stream=True).headers['Content-length'])
    # print("expected file size: ", expected_file_size)

    # If the file exists but is the wrong size, delete it.
    if os.path.isfile(file_path):
        file_size = os.path.getsize(file_path)
        # print("existing file size: ", file_size)
        if (file_size < expected_file_size):
            print("Deleting corrupted download of {}".format(filename))
            os.remove(file_path)

    # Check if the file already exists.
    # If it exists then we assume it has also been extracted,
    # otherwise we need to download and extract it now.
    if not os.path.exists(file_path):
        # Check if the download directory exists, otherwise create it.
        if not os.path.exists(download_dir):
            os.makedirs(download_dir)

        # Download the file from the internet.
        print("Downloading {} to {} ...".format(url, file_path))
        r = requests.get(url, stream=True)
        with open(file_path, 'wb') as f:
            shutil.copyfileobj(r.raw, f)

        print()
        print("Download finished.")

        print("Extracting {} to {} ...".format(file_path, download_dir))
        if file_path.endswith(".zip"):
            # Unpack the zip-file.
            zipfile.ZipFile(file=file_path, mode="r").extractall(download_dir)
        elif file_path.endswith((".tar.gz", ".tgz")):
            # Unpack the tar-ball.
            tarfile.open(name=file_path, mode="r:gz").extractall(download_dir)

        print("Extraction finished.")
    else:
        print("Data has apparently already been downloaded and unpacked.")


def maybe_download(url, download_dir):
    """
    Download the url if it doesn't already exist.
    :param url:
        Internet URL for the tar-file to download.
        Example: "https://www.cs.toronto.edu/~kriz/cifar-10-python.tar.gz"
    :param download_dir:
        Directory where the downloaded file is saved.
        Example: "data/CIFAR-10/"
    :return:
        Nothing.
    """

    # Filename for saving the file downloaded from the internet.
    # Use the filename from the URL and add it to the download_dir.
    filename = url.split('/')[-1]
    file_path = os.path.join(download_dir, filename)

    # Fetch file size.
    expected_file_size = int(requests.get(url, stream=True).headers['Content-length'])
    # print("expected file size: ", expected_file_size)

    # If the file exists but is the wrong size, delete it.
    if os.path.isfile(file_path):
        file_size = os.path.getsize(file_path)
        # print("existing file size: ", file_size)
        if (file_size < expected_file_size):
            print("Deleting corrupted download of {}".format(filename))
            os.remove(file_path)

    # Check if the file already exists.
    # If it exists then we assume it has also been extracted,
    # otherwise we need to download and extract it now.
    if not os.path.exists(file_path):
        # Check if the download directory exists, otherwise create it.
        if not os.path.exists(download_dir):
            os.makedirs(download_dir)

        # Download the file from the internet.
        print("Downloading {} to {} ...".format(url, file_path))
        r = requests.get(url, stream=True)
        with open(file_path, 'wb') as f:
            shutil.copyfileobj(r.raw, f)

        print()
        print("Download finished.")
    else:
        print("Data has apparently already been downloaded.")


##def download_and_extract():
##    """
##    Download and extract the CIFAR-10 data-set if it doesn't already exist
##    in data_path (set this variable first to the desired path).
##    """
##    maybe_download_and_extract(data_url, data_path)
##    download(model_url, filename, model_path)

def download_cifar10(cifar10_base_path):
    cifar10_url = "https://www.cs.toronto.edu/~kriz/cifar-10-python.tar.gz"
    maybe_download_and_extract(cifar10_url, cifar10_base_path)

def get_cifar10_data_path(cifar10_base_path):
    return os.path.join(cifar10_base_path, "cifar-10-batches-py")

def download_vgg16(vgg16_base_path):
    vgg16_url = "http://data.mxnet.io/models/imagenet/vgg/vgg16-symbol.json"
    maybe_download(vgg16_url, vgg16_base_path)

def get_vgg16_model_path(vgg16_base_path):
    return os.path.join(vgg16_base_path, "vgg16-symbol.json")

def download_stl10(stl10_base_path):
    stl10_url = "http://ai.stanford.edu/~acoates/stl10/stl10_binary.tar.gz"
    maybe_download_and_extract(stl10_url, stl10_base_path)

def get_stl10_data_path(stl10_base_path):
    return os.path.join(stl10_base_path, "stl10_binary")
