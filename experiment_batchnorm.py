from datetime import datetime
from pytz import timezone
import subprocess
import sys

# Current timezone.
tz = timezone('EST')

# Batch script for running LR/Batch Size experiments.

if __name__ == "__main__":
    lr_base = 0.1
    lr_decay_factor = 0.1
    lr_decay_epoch = "20,30"
    batch_size = 100

    for batch_norm_after_relu in range(1):
        for num_conv_batch_norm_sets in range(6):
            # Get time in this timezone.
            timestamp = datetime.now(tz).strftime("%Y%m%dT%H%M%S")
            
            # Generate experiment name.
            experiment_name = "{}_numconvbatchnormsets_{}_batchnormafterrelu_{}_lrbase_{}_lrdecayepoch_{}_batchsize_{}".format(
                timestamp, num_conv_batch_norm_sets, batch_norm_after_relu, lr_base, lr_decay_epoch, batch_size)
            print("Running experiment {}".format(experiment_name))
            
            args = [
                "python", "process.py",
                experiment_name,
                "--batch-size", str(batch_size),
                "--lr-base", str(lr_base),
                "--lr-decay-epochs", str(lr_decay_epoch),
                "--lr-decay-factor", str(lr_decay_factor),
                "--fc-no-dropout", "--fc-batch-norm",
                "--num-conv-batch-norm-sets", str(num_conv_batch_norm_sets)
            ]
            if batch_norm_after_relu:
                args.append("--batch-norm-after-relu")
            
    
            try:
                print(" ".join(args))
                subprocess.check_call(args)
            except subprocess.CalledProcessError:
                print("FAILURE ON " + " ".join(args))
                sys.exit(1)
