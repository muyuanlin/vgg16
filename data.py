import im2rec
import mxnet as mx
from mxnet.io import DataBatch, DataIter
import numpy as np


def add_data_args(parser):
    data = parser.add_argument_group('Data', 'Main arguments for input processing')
    data.add_argument('experiment_name', type=str,
                      help='name for experiment (will be used as prefix for generated files)')
    data.add_argument('--num-epoch', type=int, default=40,
                      help='number of epochs')
    data.add_argument('--batch-size', type=int, default=500,
                      help='batch size for training/validation/testing')
    data.add_argument('--lr-base', type=float, default=0.1,
                      help='initial learning rate')
    data.add_argument('--lr-decay-factor', type=float, default=0.1,
                      help='amount to decrease learning rate by at every decay epoch')
    data.add_argument('--lr-decay-epochs', type=str, default='20,30,60,90',
                      help='epochs at which to multiply the learning rate by the decay factor')
    data.add_argument('--pad-size', type=int, default=0,
                      help='padding the input image (probably shouldn\'t be changed)')
    data.add_argument('--image-shape', type=str, default='3,32,32',
                      help='the image shape feed into the network, e.g. (3,32,32) (probably shouldn\'t be changed)')
    data.add_argument('--rgb-mean', type=str, default='123.68,116.779,103.939',
                      help='a tuple of size 3 for the mean rgb (probably shouldn\'t be changed)')
    data.add_argument('--data-nthreads', type=int, default=4,
                      help='number of threads for data decoding')
    return data


def add_model_args(parser):
    model = parser.add_argument_group('Model', 'Arguments for model modifications')
    model.add_argument('--fc-no-dropout', action='store_true',
                       help='flag to remove dropout from fully-connected layers')
    model.add_argument('--num-conv-dropout-sets', type=int, default=0,
                      help='number of later conv sets to add dropout to (5 sets in total, so can range from 0-5)')
    model.add_argument('--fc-dropout-p', type=float, default=0.5,
                      help='dropout p-value for fully-connected layers')
    model.add_argument('--conv-dropout-p', type=float, default=0.1,
                      help='dropout p-value for convolutional layers')

    model.add_argument('--fc-batch-norm', action='store_true',
                       help='flag to add batch norm to fully-connected layers')
    model.add_argument('--num-conv-batch-norm-sets', type=int, default=0,
                      help='number of later conv sets to add batch norm to (5 sets in total, so can range from 0-5)')
    model.add_argument('--batch-norm-after-relu', action='store_true',
                       help='flag to do batch norm after relu')

    return model


def add_data_aug_args(parser):
    aug = parser.add_argument_group(
        'Image augmentations', 'Arguments for data augmentation operations')
    aug.add_argument('--aug-level', type=int, default=0,
                     help='augmentation level for defaults (overridden by actual args)')
    aug.add_argument('--random-crop', type=int, default=0,
                     help='if or not randomly crop the image')
    aug.add_argument('--random-mirror', type=int, default=0,
                     help='if or not randomly flip horizontally')
    aug.add_argument('--max-random-h', type=int, default=0,
                     help='max change of hue, whose range is [0, 180]')
    aug.add_argument('--max-random-s', type=int, default=0,
                     help='max change of saturation, whose range is [0, 255]')
    aug.add_argument('--max-random-l', type=int, default=0,
                     help='max change of intensity, whose range is [0, 255]')
    aug.add_argument('--max-random-aspect-ratio', type=float, default=0,
                     help='max change of aspect ratio, whose range is [0, 1]')
    aug.add_argument('--max-random-rotate-angle', type=int, default=0,
                     help='max angle to rotate, whose range is [0, 360]')
    aug.add_argument('--max-random-shear-ratio', type=float, default=0,
                     help='max ratio to shear, whose range is [0, 1]')
    aug.add_argument('--max-random-scale', type=float, default=1,
                     help='max ratio to scale')
    aug.add_argument('--min-random-scale', type=float, default=1,
                     help='min ratio to scale, should >= img_size/input_shape. otherwise use --pad-size')
    return aug


def set_data_aug_level(aug, level):
    if level >= 1:
        aug.set_defaults(random_crop=1, random_mirror=1)
    if level >= 2:
        aug.set_defaults(max_random_h=36, max_random_s=50, max_random_l=50)
    if level >= 3:
        aug.set_defaults(max_random_rotate_angle=10, max_random_shear_ratio=0.1,
                         max_random_aspect_ratio=0.25)


def get_rec_iter(args, cifar10_base_path, stl10_base_path):
    image_shape = tuple([int(l) for l in args.image_shape.split(',')])
    rgb_mean = [float(i) for i in args.rgb_mean.split(',')]
    train = mx.io.ImageRecordIter(
        path_imgrec=im2rec.get_cifar10_train_rec(cifar10_base_path),
        path_imgidx=im2rec.get_cifar10_train_idx(cifar10_base_path),
        label_width=1,
        mean_r=rgb_mean[0],
        mean_g=rgb_mean[1],
        mean_b=rgb_mean[2],
        data_name='data',
        label_name='softmax_label',
        data_shape=image_shape,
        batch_size=args.batch_size,
        rand_crop=args.random_crop,
        max_random_scale=args.max_random_scale,
        pad=args.pad_size,
        fill_value=127,
        min_random_scale=args.min_random_scale,
        max_aspect_ratio=args.max_random_aspect_ratio,
        random_h=args.max_random_h,
        random_s=args.max_random_s,
        random_l=args.max_random_l,
        max_rotate_angle=args.max_random_rotate_angle,
        max_shear_ratio=args.max_random_shear_ratio,
        rand_mirror=args.random_mirror,
        preprocess_threads=args.data_nthreads,
        shuffle=True)
    val = mx.io.ImageRecordIter(
        path_imgrec=im2rec.get_cifar10_test_rec(cifar10_base_path),
        path_imgidx=im2rec.get_cifar10_test_idx(cifar10_base_path),
        label_width=1,
        mean_r=rgb_mean[0],
        mean_g=rgb_mean[1],
        mean_b=rgb_mean[2],
        data_name='data',
        label_name='softmax_label',
        batch_size=args.batch_size,
        data_shape=image_shape,
        preprocess_threads=args.data_nthreads,
        rand_crop=False,
        rand_mirror=False)
    test = mx.io.ImageRecordIter(
        path_imgrec=im2rec.get_stl10_rec(stl10_base_path),
        path_imgidx=im2rec.get_stl10_idx(stl10_base_path),
        label_width=1,
        mean_r=rgb_mean[0],
        mean_g=rgb_mean[1],
        mean_b=rgb_mean[2],
        data_name='data',
        label_name='softmax_label',
        batch_size=args.batch_size,
        data_shape=image_shape,
        preprocess_threads=args.data_nthreads,
        rand_crop=False,
        rand_mirror=False)
    return (train, val, test)
